// IMPORTS USING COMMON JS MODULE

const simple = require('./modulesecond');
simple();

// IMPORTS USING ES6 MODULE

import { simple } from './modulesecond.js'; // This takes exactly the function name in that module
import simple2 from './modulesecond.js'; // This can take any name bcoz it is being exported as default
simple();
simple2();

import * as a2 from './modulesecond.js'; // This takes exactly the function name in that module
console.log(a2);