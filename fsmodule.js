// FS IS BUILTIN MODULE

const fs = require('fs');

// READING A FILE

fs.readFile('osmodule.js', 'utf8', (err, data) => {
    console.log(data);
});

const a = fs.readFileSync('file.txt');
console.log(a.toString());

b = fs.writeFileSync('file2.txt', "This is a data");
console.log(b);

console.log("FINISHED READING FILE");

