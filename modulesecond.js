// EXPORTS USING COMMON JS MODULE

function simple2() {
    console.log("Simple");
}
function simple() {
    console.log("Simple");
}

module.exports = simple;

// EXPORTS USING ES6 MODULE

export default function simple2() {
    console.log("Simple");
}
export function simple() {
    console.log("Simple");
}

export default function simple2() {
    console.log("Simple");
}
