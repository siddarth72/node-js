const EventEmitter = require('events');

class MyEmmiter extends EventEmitter { }

const myEmmiter = new MyEmmiter();
myEmmiter.on('WaterFull', () => {
    console.log('Please Turn Off Motor');
    setTimeout(() => {
        console.log('Please Turn Off Motor, A gentle reminder!');
    }, 3000);
});
console.log('The script is running');
myEmmiter.emit('WaterFull');
console.log('The script is still running');



